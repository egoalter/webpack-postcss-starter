const webpack = require('webpack');
const path = require('path');
const prod = process.env.NODE_ENV === 'production';
const MinifyPlugin = require("babel-minify-webpack-plugin");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin');

const config = {
    entry: {
        index: './app/index',
    },
    output: {
        path: path.join(__dirname, "dist"),
        filename: prod ? '[name].entry.min.js' : '[name].entry.js',
    },
    module: {
        loaders: [{
            test: '/\.js$/',
            exclude: '/node_modules/',
            loader: 'babel-loader',
            query: {
                presets: ['es2015']
            }
        }, {
            test: /\.html$/,
            loader: 'raw-loader'
        },
        {
            test: /\.css$/,
            use: ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: [
                    { loader: 'css-loader', options: { importLoaders: 1 } },
                    'postcss-loader'
                ]
            })
        },
        {
            test: /\.(jpeg|jpg|png)$/,
            loader: 'file-loader?name=images/[name].[ext]'
        }
        ]

    },
    resolve: {
        alias: {
            vue: 'vue/dist/vue.js'
        },
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jquery: 'jquery',
            'window.jQuery': 'jquery',
            jQuery: 'jquery'
        }),
        new ExtractTextPlugin("all.min.css")
    ]
};

if (prod) {
    config.plugins.push(
        new MinifyPlugin(),
        new CleanWebpackPlugin(['dist'])
    )
}

module.exports = config;