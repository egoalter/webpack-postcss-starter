## Разворачивание проекта на сервер

`npm install `

`npm install -g webpack `

`npm install -g live-server `

`webpack `

`live-server `

Если не выставлена NODE_ENV, проект соберется в development-mode
. Чтобы минифицировать js, необходимо:
 - для Windows выполнить команду `set NODE_ENV=production`
 - для MacOs && Linux  выполнить команду `export NODE_ENV=production`
 
Если запускаете в production-mode, то необходимо в index.html поменять путь к файлу. 