import Vue from 'vue';
import GiftPopup from './components/GiftPopupComponent';
import './styles/app.css';

new Vue({
    el: '#main',
    components: {
        'gift-popup-component': GiftPopup
    }

})