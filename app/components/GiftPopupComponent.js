import Vue from 'vue';
import $ from 'jquery';
import template from '../templates/gift-popup.html';
import Gift1 from '../images/gift1.png';
import Gift2 from '../images/gift2.png';
import Gift3 from '../images/gift3.png';
import Gift4 from '../images/gift4.png';
import Gift5 from '../images/gift5.png';

const GiftPopup = Vue.extend({
    template,
    data(){
        return {
            isOpen: false,
            hasGift: false,
            gifts: [
                Gift1, Gift2, Gift3, Gift4, Gift5
            ],
            activeGift: ""
        }
    },
    methods: {
        getGift(){
            let self = this,
                uri = '/test.php';
            $.post(uri)
                .success((data) => {
                    if (data.gift){
                        self.activeGift = self.gifts[data.giftId - 1]
                        self.hasGift = true;
                    }
                 })
                .error((error) => {
                    console.log(error)
                })
        }
    }

})

export default GiftPopup;